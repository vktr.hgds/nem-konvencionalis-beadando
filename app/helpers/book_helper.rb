module BookHelper

  include ApplicationHelper

  def show_book_message(title, isbn, category, price_lower, price_upper, author, publisher, available)
    msg = "<div class='alert alert-danger'>Nincs találat a következő kifejezésre: "
    msg +=  "<br>Cím: <b>#{h title}</b>" unless title.blank?
    # msg += ", " unless address.blank? && remarks.blank? && office_phone.blank? # vessző helyes használata miatt
    msg +=  "<br>ISBN: <b>#{h isbn}</b>" unless isbn.blank?
    msg +=  "<br>Kategória: <b>#{h category}</b>" unless category.blank?
    msg +=  "<br>Ár minimum: <b>#{h price_lower} Ft</b>" unless price_lower.blank?
    msg +=  "<br>Ár maximum: <b>#{h price_upper} Ft</b>" unless price_upper.blank?
    msg +=  "<br>Szerző: <b>#{h Author.find_by_id(author)&.first_name || "Nem található az adatbázisban!"}  </b>" unless author.blank?
    msg +=  "<b>#{h Author.find_by_id(author)&.last_name || '' } </b>" unless author.blank?
    msg +=  "<br>Kiadó <b>#{h Publisher.find_by_id(publisher)&.name || "Nem található az adatbázisban!" } </b>" unless publisher.blank?

    icon = available == "igen" ? is_available?(true, 16) : is_available?(false, 16)
    msg +=  "<br>Elérhető: <b>#{icon}</b>"
    msg += "</div>"
    msg.html_safe
  end

  def checkbox_available?(clicked, available)
    clicked ? available : true
  end

end
