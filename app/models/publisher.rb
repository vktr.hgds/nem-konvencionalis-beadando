class Publisher < ApplicationRecord
  has_many :books, dependent: :destroy

  scope :find_all_by, -> (name, address) {
    @publisher_list = Publisher.all
    @publisher_list = @publisher_list.where("name LIKE ?", "%#{name}%" ) if name.present?
    @publisher_list = @publisher_list.where("address LIKE ?", "%#{address}%" ) if address.present?
  }

  validates :name, :presence => {:message => "Kérjük, válasszon ki egy nevet!" }
  validates :address, :presence => {:message => "Kérjük, válasszon ki egy címet!" }

  def full_publisher_name
    "#{name}"
  end

end
