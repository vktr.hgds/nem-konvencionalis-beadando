class Book < ApplicationRecord
  belongs_to :publisher
  belongs_to :author

  validates :publisher_id, :presence => {:message => "Kérjük, válasszon ki egy kiadót!" }
  validates :author_id, :presence => {:message => "Kérjük, válasszon ki egy szerzőt!" }

  BOOK_JOIN = Book.joins(:author, :publisher)
  BOOK_WITH_AUTHOR = Book.joins(:author).select("distinct on (author_id) *")

  def author_full_name
    "#{first_name} #{last_name}"
  end

  BOOK_WITH_PUBLISHER = Book.joins(:publisher).select("distinct on (publisher_id) *")

  def publisher_name
    "#{name}"
  end

  ALL_AUTHORS = Author.all

  def every_author
    "#{first_name} #{last_name}"
  end

  ALL_PUBLISHERS = Publisher.all

  def every_publisher
    "#{name}"
  end

  scope :find_all_by, -> (title, isbn, category, price_lower, price_upper, author, publisher, available) {
    @book_list = BOOK_JOIN.all
    @book_list = @book_list.where("title LIKE ?", "%#{title}%" ) if title.present?
    @book_list = @book_list.where("isbn LIKE ?", "%#{isbn}%" ) if isbn.present?
    @book_list = @book_list.where("category LIKE ?", "%#{category}%" ) if category.present?
    @book_list = @book_list.where("price BETWEEN ? AND ?", "#{price_lower}", "#{price_upper}" ) if price_lower.present? && price_upper.present?
    @book_list = @book_list.where("author_id = ?", "#{author}" ) if author.present?
    @book_list = @book_list.where("publisher_id = ?", "#{publisher}" ) if publisher.present?
    @book_list = @book_list.where("available = ?", available)
  }

  scope :find_all_available, -> (available = true) { where("available = ?", available) }
end
