class AuthorsController < ApplicationController

  include CheckValues
  include InitializeValues

  before_action { initialize_params(title = "Szerző keresése", @author_list = Author.all) unless @clicked }
  before_action :set_author, only: [:show, :edit, :update, :destroy]
  helper_method :calculate_page_idx_upper_bound, :check_boundaries, :convert_to_int

  def search_authors
    first_name = params[:first_name]
    last_name = params[:last_name]

    @per_page = convert_to_int(params[:per_page].to_i, 5)
    @per_page = 5 if @per_page.blank? || @per_page > 10 || @per_page < 3

    @current_page = convert_to_int(params[:page].to_i, 1)
    @current_page = check_boundaries(@current_page, calculate_page_idx_upper_bound(@author_list.count, @per_page))

    @clicked = is_clicked?(first_name, last_name)

    @author_list = Author.where("first_name LIKE ?", "%#{first_name}%") if first_name.present?
    @author_list = @author_list.where("last_name LIKE ?", "%#{last_name}%") if last_name.present?
    @author_list = @author_list.paginate(:page => @current_page, :per_page => @per_page)

    respond_to do |format|
      format.html { render :search_authors }
      format.xml { render :xml => @author_list }
      format.json { render :json => @author_list }
    end
  end

  # felugro ablak(modal) megnyitasa uj oldal helyett
  def show
    respond_to do |format|
      format.js
    end
  end

  def edit; end

  def new
    @author = Author.new
  end

  def create
    @author = Author.new(valid_params)
    @author.id = Author.all.pluck(:id).max + 1
    if @author.save
      redirect_to search_authors_path, notice: "A szerző <b>(#{h @author.last_name} #{h @author.first_name})</b> létrehozása sikeresen megtörtént!".html_safe
    else
      render :new
    end
  end

  def update
    if @author.update(valid_params)
      redirect_to search_authors_path, notice: "A szerző <b>(#{h @author.last_name} #{h @author.first_name})</b> módosítása sikeresen megtörtént!".html_safe
    else
      render :edit
    end
  end

  def destroy
    @author.destroy
    redirect_to search_authors_path, notice: "A szerző <b>(#{h @author.last_name} #{h @author.first_name})</b> törlése sikeresen megtörtént!".html_safe
  end

  private

  def valid_params
    params.require(:author).permit(:last_name, :first_name)
  end

  # edit, destroy, update, show előtt lefutó metódus, amely a kódduplikálás ellen van
  def set_author
    @author = Author.find_by_id(params[:id])
  end

end


