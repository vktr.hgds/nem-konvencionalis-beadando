module PublisherHelper

  include ApplicationHelper

  def show_message(name, address)
    msg = "<div class='alert alert-danger'>Nincs találat a következő kifejezésre: "
    msg +=  "<br>Név: <b>#{h name}</b>" unless name.blank?
    msg +=  "<br>Cím: <b>#{h address}</b>" unless address.blank?
    msg += "</div>"
    msg.html_safe
  end

end