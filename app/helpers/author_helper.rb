module AuthorHelper

  include ApplicationHelper

  def show_author_message(last_name, first_name)
    msg = "<div class='alert alert-danger'>Nincs találat a következő kifejezésre: "
    msg +=  "<br>Vezetéknév: <b>#{h last_name}</b>" unless last_name.blank?
    msg +=  "<br>Keresztnév: <b>#{h first_name}</b>" unless first_name.blank?
    msg += "</div>"
    msg.html_safe
  end

end