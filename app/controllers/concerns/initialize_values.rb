module InitializeValues

  extend ActiveSupport::Concern

  def initialize_params(title = "Kezdőlap", clicked_val = false, active_val = true, per_page_val = 5, default_sql_query)
    @clicked = clicked_val
    @active = active_val
    @per_page = per_page_val
    @current_title = title.to_s
    default_sql_query
  end

end