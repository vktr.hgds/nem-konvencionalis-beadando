class Author < ApplicationRecord
  has_many :books, dependent: :destroy

  scope :find_all_by, -> (last_name, first_name) {
    @author_list = Author.all
    @author_list = @author_list.where("last_name LIKE ?", "%#{last_name}%" ) if last_name.present?
    @author_list = @author_list.where("first_name LIKE ?", "%#{first_name}%" ) if first_name.present?
  }

  validates :last_name, :presence => {:message => "Kérjük, válasszon ki egy vezetéknevet!" }
  validates :first_name, :presence => {:message => "Kérjük, válasszon ki egy keresztnevet!" }

  def full_author_name
    "#{first_name} #{last_name}"
  end
end
