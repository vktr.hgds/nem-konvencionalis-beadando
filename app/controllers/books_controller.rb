class BooksController < ApplicationController

  include CheckValues
  include InitializeValues

  before_action { initialize_params(title = "Könyv keresése", @book_list = Book::BOOK_JOIN.find_all_available) unless @clicked }
  before_action :set_profession, only: [:show, :edit, :update, :destroy]
  helper_method :calculate_page_idx_upper_bound, :check_boundaries, :convert_to_int

  InputParams = Struct.new(:author_id, :publisher_id)

  def search_books
    hash = params[:input]
    @input = InputParams.new(*hash&.values_at(*InputParams.members))

    title = params[:title]
    isbn = params[:isbn]
    category = params[:category]
    price_lower = convert_to_int(params[:price_lower], Book.minimum(:price)).to_i
    price_upper = convert_to_int(params[:price_upper], Book.maximum(:price)).to_i
    author = @input.author_id #convert_to_int(@input.author_id, Author.maximum(:id).to_i + 1).to_i if @input.author_id.present?
    publisher = @input.publisher_id #convert_to_int(@input.publisher_id, Publisher.maximum(:id).to_i + 1).to_i if @input.publisher_id.present?

    if price_lower > price_upper
      tmp = price_upper
      price_upper = price_lower
      price_lower = tmp
    end

    params[:price_lower] = price_lower if params[:price_lower].present?
    params[:price_upper] = price_upper if params[:price_upper].present?

    Rails.logger.info "pricelower: #{price_lower}"
    Rails.logger.info "priceupper: #{price_upper}"

    @per_page = convert_to_int(params[:per_page].to_i, 5)
    @per_page = 5 if @per_page.blank? || @per_page > 10 || @per_page < 3

    @current_page = convert_to_int(params[:page].to_i, 1)
    @current_page = check_boundaries(@current_page, calculate_page_idx_upper_bound(@book_list.count, @per_page))


    @clicked = is_clicked?(params[:title], params[:isbn], params[:category], params[:price_lower], params[:price_upper], params[:available])
    @available = is_available?(params[:available], @clicked)

    @book_list = Book::BOOK_JOIN.find_all_by(title, isbn, category, price_lower, price_upper, author, publisher, @available) if @clicked
    @book_list = @book_list.paginate(:page => @current_page, :per_page => @per_page)

    respond_to do |format|
      format.html { render :search_books }
      format.xml { render :xml => @book_list }
      format.json { render :json => @book_list }
    end
  end

  # felugro ablak(modal) megnyitasa uj oldal helyett
  def show
    respond_to do |format|
      format.js
    end
  end

  def edit; end

  def new
    @book = Book.new
  end

  def create
    @book = Book.new(valid_params)
    @book.id = Book.all.pluck(:id).max + 1
    if @book.save
      redirect_to search_books_path, notice: "A könyv <b>(#{h @book.title})</b> létrehozása sikeresen megtörtént!".html_safe
    else
      render :new
    end
  end

  def update
    if @book.update(valid_params)
      redirect_to search_books_path, notice: "A könyv <b>(#{h @book.title})</b> módosítása sikeresen megtörtént!".html_safe
    else
      render :edit
    end
  end

  def destroy
    @book.destroy
    redirect_to search_books_path, notice: "A könyv <b>(#{h @book.title})</b> törlése sikeresen megtörtént!".html_safe
  end

  private

  def valid_params
    params.require(:book).permit(:title, :isbn, :category, :price, :available, :author_id, :publisher_id)
  end

  # edit, destroy, update, show előtt lefutó metódus, amely a kódduplikálás ellen van
  def set_profession
    @book = Book.find_by_id(params[:id])
  end

end

