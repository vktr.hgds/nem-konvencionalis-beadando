module CheckValues

  extend ActiveSupport::Concern

  def convert_to_int(value, default_val)
    begin
      Integer(value)
    rescue
      value = default_val
    end
    value
  end

  def check_boundaries(value, lower_bound = 1, upper_bound)
    if value > upper_bound
      value = upper_bound
    elsif value < lower_bound
      value = lower_bound
    end
    value
  end

  def calculate_page_idx_upper_bound(list_size, per_page)
    ((list_size / per_page) + (list_size % per_page > 0 ? 1 : 0))
  end

  def is_clicked?(*params_list)
    params_list.all?(&:nil?) ? false : true
  end

  def is_available?(available, clicked)
    if available != "igen" && clicked
      false
    elsif available == "igen" && clicked
      true
    end
  end

end

