class CreateBooks < ActiveRecord::Migration[6.0]
  def change
    create_table :books do |t|
      t.references :publisher, null: false, foreign_key: true
      t.string :title
      t.string :isbn
      t.string :category
      t.references :author, null: false, foreign_key: true
      t.integer :number_of_pages
      t.string :short_description
      t.integer :price

      t.timestamps
    end
  end
end
