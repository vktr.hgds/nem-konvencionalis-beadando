module ApplicationHelper

  def current_index(idx, list_size, current_page, per_page)
    current_page = convert_to_int(current_page, 1)
    current_page = check_boundaries(current_page, calculate_page_idx_upper_bound(list_size, per_page))
    (idx + 1) + (per_page * (current_page.to_i - 1))
  end

  def set_row_background(idx)
    if idx % 2 == 0
      '<tr style="background: #F0F0F0">'.html_safe
    else
      '<tr style="background: #F7F7F7">'.html_safe
    end
  end

  def successful_action_msg(notice)
    if notice
      "<div id=mainAlertMessage class='alert alert-success alert-dismissible'>
         <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
          <strong>Sikeres művelet!</strong> #{notice}
      </div>".html_safe
    end
  end

  def is_available?(available, size)
    icon = "<i class='fa fa-check-circle' style='font-size: #{size}px; color: green;'></i>" if available
    icon = "<i class='fa fa-times-circle' style='font-size: #{size}px; color: red;'></i>" unless available
    icon.html_safe
  end

end
