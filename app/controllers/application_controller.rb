class ApplicationController < ActionController::Base

  def param_to_int(param, default_int_val)
    begin
      value = Integer(params[param])
    rescue
      value = default_int_val
    end
    value
  end

end
