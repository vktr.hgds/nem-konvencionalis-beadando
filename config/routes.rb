Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :books
  get 'search_books' => 'books#search_books'

  resources :authors
  get 'search_authors' => 'authors#search_authors'

  resources :publishers
  get 'search_publishers' => 'publishers#search_publishers'

  root 'books#search_books'
end
