class PublishersController < ApplicationController

  include CheckValues
  include InitializeValues

  before_action { initialize_params(title = "Kiadó keresése", @publisher_list = Publisher.all) unless @clicked }
  before_action :set_publisher, only: [:show, :edit, :update, :destroy]
  helper_method :calculate_page_idx_upper_bound, :check_boundaries, :convert_to_int

  def search_publishers
    name = params[:name]
    address = params[:address]

    @per_page = convert_to_int(params[:per_page].to_i, 5)
    @per_page = 5 if @per_page.blank? || @per_page > 10 || @per_page < 3

    @current_page = convert_to_int(params[:page].to_i, 1)
    @current_page = check_boundaries(@current_page, calculate_page_idx_upper_bound(@publisher_list.count, @per_page))

    @clicked = is_clicked?(name, address)

    @publisher_list = Publisher.where("name LIKE ?", "%#{name}%") if name.present?
    @publisher_list = @publisher_list.where("address LIKE ?", "%#{address}%") if address.present?
    @publisher_list = @publisher_list.paginate(:page => @current_page, :per_page => @per_page)

    respond_to do |format|
      format.html { render :search_publishers }
      format.xml { render :xml => @publisher_list }
      format.json { render :json => @publisher_list }
    end
  end

  # felugro ablak(modal) megnyitasa uj oldal helyett
  def show
    respond_to do |format|
      format.js
    end
  end

  def edit; end

  def new
    @publisher = Publisher.new
  end

  def create
    @publisher = Publisher.new(valid_params)
    @publisher.id = Publisher.all.pluck(:id).max + 1
    if @publisher.save
      redirect_to search_publishers_path, notice: "A szerző <b>(#{h @publisher.name})</b> létrehozása sikeresen megtörtént!".html_safe
    else
      render :new
    end
  end

  def update
    if @publisher.update(valid_params)
      redirect_to search_publishers_path, notice: "A szerző <b>(#{h @publisher.name})</b> módosítása sikeresen megtörtént!".html_safe
    else
      render :edit
    end
  end

  def destroy
    @publisher.destroy
    redirect_to search_publishers_path, notice: "A szerző <b>(#{h @publisher.name})</b> törlése sikeresen megtörtént!".html_safe
  end

  private

  def valid_params
    params.require(:publisher).permit(:name, :address)
  end

  # edit, destroy, update, show előtt lefutó metódus, amely a kódduplikálás ellen van
  def set_publisher
    @publisher = Publisher.find_by_id(params[:id])
  end

end


